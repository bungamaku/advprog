package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ResumeController.class)
public class ResumeTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void resumePage() throws Exception {
        mockMvc.perform(get("/resume")).andExpect(content().string(containsString("computer science")));
    }

    @Test
    public void applicationRun() {
        Application.main(new String[]{
                "--spring.main.web-environment=false"
        });
    }
}
