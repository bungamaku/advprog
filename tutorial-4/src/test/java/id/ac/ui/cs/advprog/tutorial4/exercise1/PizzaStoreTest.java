package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PizzaStoreTest {
    @Test
    public void testNewYorkPizzaStore() {
        PizzaStore nyStore = new NewYorkPizzaStore();
        assertEquals(nyStore.orderPizza("cheese").getName(),
                "New York Style Cheese Pizza");
        assertEquals(nyStore.orderPizza("veggie").getName(),
                "New York Style Veggie Pizza");
        assertEquals(nyStore.orderPizza("clam").getName(),
                "New York Style Clam Pizza");
    }

    @Test
    public void testDepokPizzaStore() {
        PizzaStore dpStore = new DepokPizzaStore();
        assertEquals(dpStore.orderPizza("cheese").getName(),
                "Depok Style Cheese Pizza");
        assertEquals(dpStore.orderPizza("veggie").getName(),
                "Depok Style Veggie Pizza");
        assertEquals(dpStore.orderPizza("clam").getName(),
                "Depok Style Clam Pizza");
    }
}
