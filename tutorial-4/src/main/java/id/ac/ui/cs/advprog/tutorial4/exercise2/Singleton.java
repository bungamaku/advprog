package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    private static Singleton anInstance = null;

    private Singleton() {}

    public static synchronized Singleton getInstance() {
        if (anInstance == null) {
            anInstance = new Singleton();
        }
        return anInstance;
    }
}
