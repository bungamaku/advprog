package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SoftCrustDough implements Dough {
    public String toString() {
        return "SoftCrust style extra soft crust dough";
    }
}
